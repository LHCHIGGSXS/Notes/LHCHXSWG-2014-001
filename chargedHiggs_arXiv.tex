% Please use the skeleton file you have received in the 
% invitation-to-submit email, where your data are already
% filled in. Otherwise please make sure you insert your 
% data according to the instructions in PoSauthmanual.pdf
\documentclass{PoS_arXiv}

\title{Charged Higgs Bosons in the LHCHXSWG}

\usepackage{amsmath,amssymb,amsfonts,color,graphicx,cite,color,soul}


\ShortTitle{LHCHXSWG: Charged Higgs}

\author{\speaker{S. Heinemeyer}\\ %\thanks{A footnote may follow.}\\
        Instituto de F\'isica de Cantabria (CSIC-UC), Santander, Spain\\
        E-mail: \email{Sven.Heinemeyer@cern.ch}}

%\author{Another Author\\
%        Affiliation\\
%        E-mail: \email{...}}

\abstract{Searches for charged Higgs bosons are an integral part of current
  and future investigations at the LHC. The LHC Higgs Cross Section
  Working Group 
  (LHCHXSWG) was created to provide cross sections, branching ratios,
  analysis strategies etc.\ for Higgs boson searches at the LHC.
  We briefly review progress and results for charged Higgs bosons in and
  for the LHCHXSWG.}

\FullConference{Prospects for Charged Higgs Discovery at Colliders - CHARGED
  2014,\\ 
		16-18 September 2014\\
		Uppsala University, Sweden}


\include{paperdef}
\graphicspath{{figs/}}

\newcommand{\simMH}{125}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

A major goal of the 
particle physics program at the high energy frontier,
currently being pursued at the CERN Large Hadron Collider (LHC),
is to unravel the nature of electroweak symmetry breaking (EWSB).
While the existence of the massive electroweak gauge bosons ($W^\pm,Z$),
together with the successful description of their behavior by
non-abelian gauge theory, 
requires some form of EWSB to be present in nature, 
the underlying dynamics remained unknown for several decades. 
An appealing theoretical suggestion for such dynamics is the Higgs mechanism
\cite{higgs-mechanism}, which 
implies the existence of one or more 
Higgs bosons (depending on the specific model considered).
Therefore, the search for a Higgs boson was considered a major cornerstone
in the physics program of the LHC.

The spectacular discovery of a Higgs-like particle 
with a mass around $\MH \simeq \simMH \gev$, which has been announced
by ATLAS \cite{ATLASdiscovery} and CMS~\cite{CMSdiscovery}, marks a
milestone of an effort that has been ongoing for almost half a century
and opens up a new era of particle physics.  
Both ATLAS and CMS reported a clear excess in the two photon channel, as
well as in the $ZZ^{(*)}$ channel. The discovery was further 
corroborated, though not with high significance, by the
$WW^{(*)}$ channel and by the final Tevatron results~\cite{TevHiggsfinal}.
Latest ATLAS/CMS results, also for evidence on the Higgs decay into
fermions can be found in \citeres{ATLAS-Higgs-WWW,CMS-Higgs-WWW}. 

\medskip
Many theoretical models employing the Higgs mechanism in
order to account for electroweak symmetry breaking
have been studied in the literature, of which 
the most popular ones are the Standard Model (SM)~\cite{sm}  
and the Minimal Supersymmetric Standard Model (MSSM)~\cite{mssm}, 
The newly discovered particle can be interpreted as the SM Higgs boson.
The MSSM has a richer Higgs sector, containing two neutral $\cp$-even,
one neutral $\cp$-odd and two charged Higgs bosons. 
The newly discovered particle can also be interpreted as the light (or the
the heavy) $\cp$-even state~\cite{Mh125}. 
Among alternative theoretical models beyond the SM and the MSSM,
the most prominent are  
the (more general) Two Higgs Doublet Model (2HDM)~\cite{thdm,thdm-types}, 
non-minimal supersymmetric extensions of the SM 
(e.g.\ extensions of the MSSM by an extra singlet
superfield \cite{NMSSM-etc}), or models involving Higgs
triplets~\cite{triplet}. 
Many of these models not only predict more than one Higgs boson, but they
predict electrically charged Higgs bosons.


\medskip
The ATLAS and CMS analyses leading to the conclusion that (within the
uncertainties) the newly discovered particle can be interpreted as the SM
Higgs boson requires, besides the obvious experimental data, also precise
theory predictions for the SM Higgs boson cross section, branching ratios,
angular distributions as well as strategies how to extract certain
``measurements'' (e.g.\ coupling strength factors) from the data. 
In this respect it is crucial that ATLAS and CMS not only use predictions with
highest precision, but in particular that they use the {\em same} theory
predictions, the {\em same} strategies for the extraction of
``measurements''. Only then it is possible to readily compare ATLAS and
CMS results, and in the future combine them. 
To ensure this, in the year 2010 the 
``LHC Higgs Cross Section Group'' (LHCHXSWG)~\cite{LHCHXSWG-www1} was founded. 
This group, formed of theoretical and experimental physicists,
officially takes care of providing cross section and 
branching ratio predictions (including uncertainty evaluations), as well
as the strategies for the extraction of, e.g., coupling strength factors
from experimental data~\cite{YR1,YR2,YR3,HiggsRecommendation}. 
While initially the SM Higgs boson was in the
focus of the LHCHXSWG, soon also models beyond the SM (BSM) were
investigated (see also \citere{LHCHXSWG-www2}). In particular within the
MSSM cross sections and branching 
ratios for the extended Higgs sector have been evaluated, see, e.g.,
\citere{MSSMHiggsXS} for an example on the neutral Higgs production
cross sections. Latest results can be found at \citere{LHCHXSWG-www3}.

As discussed above, electrically charged Higgs bosons form a natural
part of many BSM models. 
The charged Higgs bosons of the MSSM (or a more general 2HDM)
have been searched at LEP, the Tevatron and the LHC, and
will be searched for (or hopefully analyzed at) a Linear Collider such
as ILC or CLIC. The LEP searches~\cite{ADLOchargedHiggs}
yielded a robust bound of
$\MHp \gsim 80 \gev$~\cite{LEPchargedHiggs}.
The Tevatron bounds~\cite{Tevcharged} are by now superseeded by the LHC
charged Higgs searches~\cite{LHCcharged}.
At the ILC, if the charged Higgs is in the
kinematical reach, a high-precision determination of the
charged Higgs boson properties will be
possible~\cite{ILC-TDR,MHpLHCILCnewer}.
Here, besides some basics, we briefly review activities and results
obtained within and for the LHCHXSWG regarding charged Higgs bosons, 
which will mainly concern the 2HDMs and the MSSM.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\section{``Light'' and ``heavy'' charged Higgs bosons}
%\label{sec:lightheavy}

\bigskip
Within the 2HDM and the MSSM 
the main production channels of charged Higgs bosons at the LHC are
\begin{align}
\label{pp2Hpm}
pp \to t\bar t \; + \; X, \quad
t \bar t \to t \; H^- \bar b \mbox{~~or~~} H^+ b \; \bar t , \\
\label{gb2Hpm}
gb \to H^- t \mbox{~~or~~} g \bar b \to H^+ \bar t~ \quad \mbox{(5FS)}\,, \\
\label{gg2Hpm}
gg/q\bar q \to H^- t \bar b \mbox{~~or~~} 
gg/q\bar q \to H^+ \bar t b~ \quad \mbox{(4FS)}\,. 
\end{align}
The decay used in the analysis to detect the charged Higgs boson is
\begin{align}
H^\pm \; \to \; \tau \nu_\tau \; \to \; {\rm hadrons~}\nu_\tau. 
\label{Hdec}
\end{align}

The \ul{``light charged Higgs boson''} is characterized by $\MHp < \mt$. 
The main production channel is given in \refeq{pp2Hpm}. Close to
threshold also \refeq{gb2Hpm} contributes. The relevant (i.e.\
detectable) decay channel is given by \refeq{Hdec}.


The \ul{``heavy charged Higgs boson''} is characterized by $\MHp \gsim \mt$.
Here \refeq{gb2Hpm} in the ``five flavor scheme'' (5FS) and/or 
\refeq{gg2Hpm} in the ``four flavor scheme'' (4FS) gives the largest
contribution to the production cross section, and very close to 
threshold \refeq{pp2Hpm} can contribute somewhat. The relevant decay
channel is again given in \refeq{Hdec}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Charged Higgs bosons in 2HDMs}
\label{sec:2hdm}

The 2HDM can be classified in types I-IV~\cite{thdm-types}, where the
MSSM, see \refse{sec:mssm} at the tree-level contains a 2HDM type~II.
The relevant free (input) parameters are $\MHp$ and the ratio of the two
vacuum expectation values, $\tb \equiv v_2/v_1$.
Analyses at ATLAS and CMS
in the case of light charged Higgs bosons in the context of 2HDMs
evaluate the production cross section from $\si(pp \to t \bar t + X)$ as
evaluated in the SM at the NNLO level~\cite{ppttNNLO}. Limits are then
presented for $\br(t \to H^\pm b)$ as a function of the charged Higgs boson
mass,~$\MHp$. 

\medskip
For heavy charged Higgs bosons, $\MHpm \gsim \mt$,
associated production $pp \to tb H^\pm{\rm + X}$ is the
dominant production mode. 
Two different formalisms can be employed to calculate the cross
section for associated $tb H^\pm$ production.  In the
four-flavor scheme (4FS) with no $b$~quarks in the initial state,
the lowest-order QCD production processes are given in \refeq{gg2Hpm}.

On the other hand, potentially
large logarithms $\propto \ln(\mu_{\rm F}/\mb)$ (where $\mu_{\rm F}$
denotes the factorization scale), which arise from
the splitting of incoming gluons into nearly collinear $b \bar b$
pairs, can be summed to all orders in perturbation theory by
introducing bottom parton densities, i.e.\ in the 
five flavor scheme (5FS)~\cite{Barnett:1987jw}, see \refeq{gb2Hpm}.


To all orders in perturbation theory the four-
and five-flavor schemes are identical, but the way of ordering the
perturbative expansion is different, and the results do not match
exactly at finite order. For more details see \citere{YR2} and
references therein.
%
A simple and pragmatic formula for the combination of the 
four- and five-flavor scheme calculations of 
bottom-quark associated Higgs-boson production
has been suggested in \citere{Harlander:2011aa}, the so-called 
``Santander matching''. 
The main idea behind this matching scheme is the following:
The 4FS and 5FS calculations provide the unique description of the
cross section in the asymptotic limits $\MH/\mb \to 1$ and
$\MH/\mb \to \infty$, respectively (where $\MH$ denotes a
generic Higgs boson mass, i.e.\ the arguments are valid for the neutral
as well as for the charged Higgs production). 
The two approaches are combined in
such a way that they are given a weight, depending on the value
of the Higgs-boson mass. Since the difference between the 4FS and the 5FS
is logarithmic, the dependence of their relative importance
on $\MH$ should be controlled by a logarithmic term.
Consequently, the proposal for the ``Santander matching''
reads~\cite{Harlander:2011aa}, 
\begin{align}
\si^\text{matched} &= \frac{\si^\text{4FS} + t\,\si^\text{5FS}}{1+t}\,,
%\end{align}
\quad \mbox{~with the weight $w$ defined as~} \quad
%\begin{align}
t = \ln\frac{\MH}{\mb}  - 2\,,
\end{align}
and $\si^\text{4FS}$ and $\si^\text{5FS}$ denote the total
inclusive cross section in the 4FS and the 5FS, respectively.
%
The theoretical uncertainties  in the 4FS and the 5FS calculations
should be added linearly, using the
weight $t$. In this way it is ensured that the combined
error is always larger than the minimum of the two individual
errors~\cite{Harlander:2011aa}:
\begin{align}
\De\si_\pm &= \frac{\De\si_\pm^\text{4FS}
  + t\,\De\si_\pm^\text{5FS}}{1+t}\,,
\end{align}
where $\De\si_\pm^\text{4FS}$ and
$\De\si_\pm^\text{5FS}$ denote the upper/lower uncertainty limits
of the 4FS and the 5FS, respectively.

\medskip
An up-to-date determination of the next-to-leading order total cross
section in the type~II 2HDM as a function of $\MHp$ and $\tb$ has
recently been presented in 
\citere{Flechl:2014wfa}, which constitutes the official recommendation
of the LHCHXSWG for heavy charged Higgs bosons. Also included in
\citere{Flechl:2014wfa} is an estimate of the theoretical uncertainties
due to missing higher-order corrections, parton distribution functions
and physical input parameters. 
Predictions in the 4FS and 5FS were compared and reconciled through
a recently proposed scale-setting prescription. 
Applying the Santander matching the ``best'' cross section prediction
for heavy charged Higgs bosons at the LHC is provided.

An interim recommendation of the LHCHXSWG on the evaluation of cross
sections and branching ratios in the 2HDM has been presented in
\citere{thdm-lhchxswg-reco}, however, with a focus on neutral Higgs
bosons. The two codes recommended for the Higgs boson decays, 
{\tt Hdecay}~\cite{hdecay} and {\tt 2HDMC}~\cite{2hdmc} also include the
evaluation of charged Higgs boson decays in types~I-IV.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Charged Higgs bosons in the MSSM}
\label{sec:mssm}

While the MSSM contains (at the tree-level) a 2HDM type~II, due to
Supersymmetry (SUSY), special relations are enforced, and via loop
corrections the full SUSY spectrum enters the predictions.

The Higgs sector of the MSSM
contains two Higgs doublets, leading to five
physical Higgs bosons. At tree-level these are the light and heavy
$\cp$-even $h$~and $H$, the $\cp$-odd $A$ and the charged $H^\pm$. At
lowest order the Higgs sector can be described besides the SM
parameters by two additional independent
parameters, chosen to be the mass of the $A$~boson, $\MA$ 
(in the case of vanishing complex phases) and $\tb$. 
Accordingly, all other masses and couplings can be predicted at
tree-level, e.g.\ the charged Higgs boson mass
\begin{align}
\label{MHptree}
\mHp^2 &= \MA^2 + \MW^2~.
\end{align}
$M_{Z,W}$ denote the masses of the $Z$~and $W$~boson,
respectively. 
This tree-level relation receives higher-order corrections, where the loop
corrected charged Higgs-boson mass is denoted as $\MHp$. 
Three codes exist for the calculation of $\MHp$ and the various decay
widths, 
{\tt FeynHiggs}~\cite{feynhiggs,mhiggslong,mhiggsAEC,mhcMSSMlong,mhcMSSM2L,Mh-logresum}, 
{\tt CPsuperH}~\cite{cpsh}
and {\tt Hdecay}~\cite{hdecay}. 


The relation between the bottom-quark mass and the Yukawa coupling
$h_b$, which controls also the interaction between the Higgs fields and
the sbottom quarks, is affected by higher-order corrections,
summarized in the quantity $\db$~\cite{deltamb1,deltamb2,db2l}.
These, often called threshold corrections, are generated either by
gluino--sbottom one-loop diagrams (resulting in \order{\alb\als}
corrections), or by chargino--stop loops (giving
\order{\alb\alt} corrections). 
The effective Lagrangian for the charged Higgs is given by~\cite{deltamb2}
\begin{align}
\label{effL}
%\cL = \frac{g_2}{2\MW} \wz \, V_{tb} 
\cL \sim V_{tb} 
      \KKL \KL \frac{\mbms}{1 + \db} \, \tb + \frac{\mt}{\tb} \KR
           H^+ \bar{t}_L b_R \KKR + {\rm h.c.}
\end{align}
Here $V_{tb}$ denotes the $(3,3)$ element of the CKM matrix, $\mbms$ is
the running bottom quark mass, and $\mt$ is the top quark mass.
Analytically one finds $\db \propto \mu \tb$, where $\mu$ is the Higgs
mixing parameter, which is (generally) of the same size as SUSY mass
scales. 
Large positive (negative) values of $\db$ lead to a strong suppression
(enhancement) of the bottom Yukawa coupling.

\medskip
For the evaluation of the light charged Higgs production cross section
the decay $t \to H^\pm b$ has to be evaluated including SUSY loop
corrections, where the main contribution stems from \refeq{effL}. The
LHCHXSWG compared the codes {\tt FeynHiggs} and {\tt Hdecay} as shown in
\reffi{fig:lightHiggsComp}~\cite{YR2}.
The top row shows the decay width, while the bottom row contains the
result for the branching ratios. The parameters are chosen according to
the $\mhmax$~scenario~\cite{benchmark2} with $\mu$ set to $200 (1000) \gev$
in the left (right) column. One can see that the agreement between the
two codes, despite some differences in the $\db$ evaluation (see
\citere{YR2} for details) is excellent.
%In the following we assume a $\sim 6\%$ uncertainty in the top decays to 
%missing one-loop electroweak and two-loop QCD corrections.

%%%%%%%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[htb!]
\begin{center}
\includegraphics[width=0.45\textwidth]{YRHXS2_chiggs_fig1c}
\includegraphics[width=0.45\textwidth]{YRHXS2_chiggs_fig1d}\\
\includegraphics[width=0.45\textwidth]{YRHXS2_chiggs_fig2c}
\includegraphics[width=0.45\textwidth]{YRHXS2_chiggs_fig2d}
\caption{Comparison of the $\Ga(t \to H^+ b)$ (upper row) and 
$\br(t \to H^+ b)$ (lower row) between {\tt FeynHiggs} and {\tt Hdecay}.
The results are shown for various values of $\MHp$ and for 
$\mu = 200 (1000) \gev$ in the left (right) column (taken from \citere{YR2}).
}
\label{fig:lightHiggsComp}
\end{center}
%\vspace{2em}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The LHCHXSWG also estimated 
the overall uncertainty of the light charged Higgs production, evaluated
in the $\mhmax$ scenario. The result is shown in
\reffi{fig:totalUnc}~\cite{YR2}, where
\begin{align}
\si_{tt} \cdot \br(t \to b H^\pm) \cdot \br(t \to b W^\pm) \cdot 2
\end{align}
is shown for $\sqrt{s} = 7 \tev$ 
as a function of $\MHp$. The uncertainty estimate combines the
accuracies for the top quark production (parametric and intrinsic
uncertainty) and for the top quark decay (including intrinsic
uncertainties on $\db$). 
The result is shown for $\tb = 5, 10, 30, 50$. 
As can be seen, the uncertainties are still substantial. They have to be
taken into account for reliable and robust bounds on the MSSM parameter
space from the non-observation of a light charged Higgs. Conversely,
using a potential observation of a light charged Higgs for a
determination of the underlying parameters would require a substantial
reduction of the uncertainties.

%%%%%%%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[htb!]
\begin{center}
\includegraphics[width=0.70\textwidth]{sigmatt_BRtHpb_03B}
\caption{$\si_{tt} \cdot \br(t \to b H^\pm) \cdot \br(t \to b W^\pm) \cdot 2$ 
including scale and PDF uncertainties, uncertainties for missing
electroweak and QCD corrections, and $\db$-induced uncertainties for
$\sqrt{s} = 7 \tev$ (taken from \citere{YR2}).
}
\label{fig:totalUnc}
\end{center}
%\vspace{2em}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\medskip
The LHCHXSWG also provides branching ratio predictions for the MSSM
Higgs bosons, including the charged Higgs boson.
The procedure adopted by the LHCHXSWG goes as follows.
After the calculation of Higgs-boson masses and mixings from the
original SUSY input, a
combination of the results from {\tt Hdecay} and 
{\tt FeynHiggs} on the various decay channels is performed to 
obtain the most accurate result for the branching ratios currently
available. (For the general procedure, see \citere{BR}.) In a first
step, all partial widths have been calculated as accurately as
possible. Then the branching ratios have been derived from this full
set of partial widths. 
Concretely, {\tt FeynHiggs} was used for the evaluation of the
Higgs-boson masses and couplings from the original input
parameters, including corrections up to the two-loop level. 
The status of the various evaluations in {\tt FeynHiggs} and 
{\tt Hdecay} are detailed in \citere{YR2}.
The total decay width of the charged Higgs bosons is calculated as,
\begin{align}
\Gamma_{H^\pm} &= 
\phantom{+}  \Gamma^{\mathrm{FH}}_{H^\pm \to \tau \nu_\tau} 
+ \Gamma^{\mathrm{FH}}_{H^\pm \to \mu \nu_\mu} 
+ \Gamma^{\mathrm{FH}}_{H^\pm \to h W^\pm} 
+ \Gamma^{\mathrm{FH}}_{H^\pm \to H W^\pm} 
+ \Gamma^{\mathrm{FH}}_{H^\pm \to A W^\pm} \nonumber\\
&\quad 
+ \Gamma^{\mathrm{HD}}_{H^\pm \to tb}
+ \Gamma^{\mathrm{HD}}_{H^\pm \to ts}
+ \Gamma^{\mathrm{HD}}_{H^\pm \to td}
+ \Gamma^{\mathrm{HD}}_{H^\pm \to cb}
+ \Gamma^{\mathrm{HD}}_{H^\pm \to cs}
+ \Gamma^{\mathrm{HD}}_{H^\pm \to cd} \nonumber \\
&\quad
+ \Gamma^{\mathrm{HD}}_{H^\pm \to ub}
+ \Gamma^{\mathrm{HD}}_{H^\pm \to us}
+ \Gamma^{\mathrm{HD}}_{H^\pm \to ud}~,
\end{align}
followed by a corresponding evaluation of the respective branching
ratio. Decays to strange quarks or other lighter fermions have been neglected. 

Example results in the $\mh^{\rm mod+}$ scenario~\cite{benchmark4} are
given in \reffi{fig:BRHp}~\cite{YR3}. The left (right) plot show the BRs
for $\tb = 10 (50)$ as a function of $\MHp$. The various kinks visible
in the left plot  stem from the decay channels to a
chargino/neutralino pair, which are not explicitely included into the BR
predictions yet.

%%%%%%%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[htb!]
\begin{center}
\includegraphics[width=0.45\textwidth]{YRHXS3_BR_fig33}
\includegraphics[width=0.45\textwidth]{YRHXS3_BR_fig34}
\caption{Charged Higgs boson branching ratios in the 
$\mh^{\rm mod+}$ scenario~\cite{benchmark4} for $\tb = 10 (50)$ in the
  left (right) plot as a function of $\MHp$ (taken from \citere{YR3}).
}
\label{fig:BRHp}
\end{center}
%\vspace{2em}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%% F I G U R E %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Conclusions}

The LHCHXSWG forms an important part of the efforts to identify the
mechanism of EWSB at the LHC. Among many other activities, it provides cross
sections and branching ratios for charged Higgs bosons as they are
predicted by the 2HDM and/or the MSSM. Here we briefly reviewed some of
the predictions for light and heavy charged Higgs bosons, including
evaluations of the respective uncertainties.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection*{Acknowledgements}

We thank the organizers of C$H^{\mbox{}^\pm}$\hspace{-2.5mm}arged 2014
for the invitation and the, as always, pleasant and productive atmosphere.
We thank particularly many members of the LHCHXSWG, who obtained the
results reviewed here.
The work of S.H.\ is supported in part by CICYT 
(grant FPA 2013-40715-P) and by the Spanish MICINN's Consolider-Ingenio 
2010 Program under grant MultiDark CSD2009-00064.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{thebibliography}{99}

%\include{refs}
%\include{chargedHiggs_ref}

%1
\bibitem{higgs-mechanism}
        P.~Higgs,
        {\em Phys.\ Lett.} {\bf 12} (1964) 132;
        %%CITATION = PHLTA,12,132;%%
        {\em Phys.\ Rev.\ Lett.} {\bf 13} (1964) 508;
        %%CITATION = PRLTA,13,508;%%
        {\em Phys.\ Rev.} {\bf 145} (1966) 1156;
        %%CITATION = PHRVA,145,1156;%%
        F.~Englert and R.~Brout,
        {\em Phys.\ Rev.\ Lett.} {\bf 13} (1964) 321;
        %%CITATION = PRLTA,13,321;%%
        G.~~Guralnik, C.~~Hagen and T.~Kibble,
        {\em Phys.\ Rev.\ Lett.} {\bf 13} (1964) 585.
        %%CITATION = PRLTA,13,585;%%

%2
\bibitem{ATLASdiscovery} G.~Aad et al.\  [ATLAS Collaboration],
  {\em Phys.\ Lett.} {\bf B 716} (2012) 1
  [arXiv:1207.7214 [hep-ex]].
  %%CITATION = ARXIV:1207.7214;%%

%3
\bibitem{CMSdiscovery} S.~Chatrchyan et al.\  [CMS Collaboration],
  {\em Phys.\ Lett.} {\bf B 716} (2012) 30
  [arXiv:1207.7235 [hep-ex]].
  %%CITATION = ARXIV:1207.7235;%%

%4
\bibitem{TevHiggsfinal} CDF Collaboration, D\O\ Collaboration, 
  [arXiv:1207.0449 [hep-ex]].
  %%CITATION = ARXIV:1207.0449;%

%5
\bibitem{ATLAS-Higgs-WWW} ATLAS Collaboration, see:\\
  {\tt https://twiki.cern.ch/twiki/bin/view/AtlasPublic/HiggsPublicResults}~.

%6
\bibitem{CMS-Higgs-WWW} CMS Collaboration, see:\\
  {\tt https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResultsHIG}~.

%7
\bibitem{sm} S.~Glashow, 
             {\em Nucl.\ Phys.} {\bf B 22} (1961) 579; 
             %%CITATION = NUPHA,22,579;%%
             S. Weinberg, 
             {\em Phys. Rev. Lett.} {\bf 19} (1967) 19; 
             %%CITATION = PRLTA,19,1264;%%
             A. Salam, in: {\em Proceedings of the 8th Nobel 
             Symposium}, Editor N. Svartholm, Stockholm, 1968.

%8
\bibitem{mssm} H.~Nilles, 
               {\em Phys.\ Rept.} {\bf 110} (1984) 1; 
               %%CITATION = PRPLC,110,1;%%
               H.~Haber and G.~Kane, 
               {\em Phys.\ Rept.} {\bf 117} (1985) 75; 
               %%CITATION = PRPLC,117,75;%%
               R.~Barbieri, 
               {\em Riv.\ Nuovo Cim.} {\bf 11} (1988) 1. 
               %%CITATION = RNCIB,11,1;%%

%9
\bibitem{Mh125} S.~Heinemeyer, O.~St{\aa}l and G.~Weiglein, 
                {\em Phys.\ Lett.} {\bf B 710} (2012) 201
                [arXiv:1112.3026 [hep-ph]]; 
                %%CITATION = ARXIV:1112.3026;%%

%10
\bibitem{thdm} S.~Weinberg,
        Phys.\ Rev.\ Lett.\  {\bf 37} (1976) 657;
        %%CITATION = PRLTA,37,657;%%
        J.~Gunion, H.~Haber, G.~Kane and S.~Dawson,
        {\em The Higgs Hunter's Guide}
        (Perseus Publishing, Cambridge, MA, 1990),
        and references therein;
        G.~Branco et al., 
        {\em Phys.\ Rept.} {\bf 516} (2012) 1
        [arXiv:1106.0034 [hep-ph]].
        %%CITATION = ARXIV:1106.0034;%%

%11
\bibitem{thdm-types} V.~Barger, J.~Hewett and R.~Phillips,
  {\em Phys.\ Rev.} {\bf D 41} (1990) 3421.
  %%CITATION = PHRVA,D41,3421;%%

%12
\bibitem{NMSSM-etc}
        P.~Fayet, 
        {\em Nucl. Phys.} {\bf B 90} (1975) 104;
        {\em Phys. Lett.} {\bf B 64} (1976) 159;
        {\em Phys. Lett.} {\bf B 69} (1977) 489;
        {\em Phys. Lett.} {\bf B 84} (1979) 416;
        H.P.~Nilles, M.~Srednicki and D.~Wyler,
        {\em Phys. Lett.} {\bf B 120} (1983) 346;
        J.M.~Frere, D.R.~Jones and S.~Raby,
        {\em Nucl. Phys.} {\bf B 222} (1983) 11;
        J.P.~Derendinger and C.A.~Savoy, 
        {\em Nucl. Phys.} {\bf B 237} (1984) 307;
        J.~Ellis, J.~Gunion, H.~Haber, L.~Roszkowski and F.~Zwirner,
        {\em Phys. Rev.} {\bf D 39}  (1989) 844;
        M.~Drees, 
        {\em Int. J. Mod. Phys.} {\bf A 4}  (1989) 3635;
        U.~Ellwanger, C.~Hugonie and A.~Teixeira,
        {\em Phys.\ Rept.} {\bf 496} (2010) 1 
        [arXiv:0910.1785 [hep-ph]];
        %%CITATION = ARXIV:0910.1785;%%
        M.~Maniatis,
        {\em Int.\ J.\ Mod.\ Phys.} {\bf A 25} (2010) 3505
        [arXiv:0906.0777 [hep-ph]].
        %%CITATION = ARXIV:0906.0777;%%

%13
\bibitem{triplet} H.~Georgi and M.~Machacek,
  {\em Nucl.\ Phys.} {\bf B 262} (1985) 463;
  %%CITATION = NUPHA,B262,463;%%
  M.~Chanowitz and M.~Golden,
  {\em Phys.\ Lett.} {\bf 165} (1985) 105.
  %%CITATION = PHLTA,B165,105;%%

%14
\bibitem{LHCHXSWG-www1} 
 {\tt https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CrossSections2011}~.

%15
\bibitem{YR1} S.~Dittmaier et al. 
              [LHC Higgs Cross Section Working Group],
              arXiv:1101.0593 [hep-ph].
              %%CITATION = ARXIV:1101.0593;%%

%16
\bibitem{YR2} S.~Dittmaier et al. 
              [LHC Higgs Cross Section Working Group],
              arXiv:1201.3084 [hep-ph].
              %%CITATION = ARXIV:1201.3084;%%

%17
\bibitem{YR3} S.~Heinemeyer et al.
              [LHC Higgs Cross Section Working Group],
              arXiv:1307.1347 [hep-ph].
              %%CITATION = ARXIV:1307.1347;%%

%18
\bibitem{HiggsRecommendation} LHC Higgs Cross Section Working Group,
  A.~David et al., 
  arXiv:1209.0040 [hep-ph].
  %%CITATION = ARXIV:1209.0040;%%

%19
\bibitem{LHCHXSWG-www2}
 {\tt https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CrossSections}~.

%20
\bibitem{MSSMHiggsXS}
  E.~Bagnaschi, R.~Harlander, S.~Liebler, H.~Mantler, P.~Slavich and A.~Vicini,
  {\em JHEP} {\bf 1406} (2014) 167
  [arXiv:1404.0327 [hep-ph]].
  %%CITATION = ARXIV:1404.0327;%%

%21
\bibitem{LHCHXSWG-www3} 
 {\tt https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWG}~.

%22
\bibitem{ADLOchargedHiggs} A.~Heister et al.\ [ALEPH Collaboration],
                            {\em Phys.\ Lett.} {\bf B 543} (2002) 1
                            [arXiv:hep-ex/0207054];
                            %%CITATION = PHLTA,B543,1;%%
                            J.~Abdallah et al.\ [DELPHI Collaboration],
                            {\em Eur.\ Phys.\ J.} {\bf C 34} (2004) 399
                            [arXiv:hep-ex/0404012];
                            %%CITATION = EPHJA,C34,399;%%
                            P.~Achard et al.\ [L3 Collaboration],
                            {\em Phys.\ Lett.} {\bf B 575} (2003) 208
                            [arXiv:hep-ex/0309056];
                            %%CITATION = PHLTA,B575,208;%%
                            D.~Horvath {}[OPAL Collaboration],
                            {\em Nucl.\ Phys.} {\bf A 721} (2003) 453.
                            %%CITATION = NUPHA,A721,453;%%

%23
\bibitem{LEPchargedHiggs} G.~Abbiendi et al. [ALEPH and DELPHI
  and L3 and OPAL and LEP Collaborations], 
  {\em Eur.\ Phys.\ J.} {\bf C 73} (2013) 2463
  [arXiv:1301.6065 [hep-ex]].
  %%CITATION = ARXIV:1301.6065;%%

%24
\bibitem{Tevcharged} T.~Aaltonen et al.  [CDF Collaboration],
                     {\em Phys.\ Rev.\ Lett.}  {\bf 103} (2009) 101803
                     [arXiv:0907.1269 [hep-ex]];
                     %%CITATION = ARXIV:0907.1269;%%
                     V.~Abazov et al. [D\O Collaboration],
                     {\em Phys.\ Lett.} {\bf B 682} (2009) 278
                     [arXiv:0908.1811 [hep-ex]];
                     %%CITATION = ARXIV:0908.1811;%%
                     P.~Gutierrez [CDF and D\O Collaborations],
                     PoS CHARGED {\bf 2010} (2010) 004.
                     %%CITATION = POSCI,CHARGED2010,004;%%

%25
\bibitem{LHCcharged} G.~Aad et al. [ATLAS Collaboration],
                     {\em JHEP} {\bf 1206} (2012) 039
                     [arXiv:1204.2760 [hep-ex]];
                     %%CITATION = ARXIV:1204.2760;%%
                     ATLAS Collaboration, 
                     ATLAS-CONF-2013-090; ATLAS-CONF-2014-050;
                     S.~Chatrchyan et al. [CMS Collaboration],
                     {\em JHEP} {\bf 1207} (2012) 143
                     [arXiv:1205.5736 [hep-ex]];
                     %%CITATION = ARXIV:1205.5736;%%
                     CMS Collaboration, 
                     CMS-HIG-13-035; CMS-HIG-14-020.

%26
\bibitem{ILC-TDR}
H.~Baer et al.,
%{\it The International Linear Collider Technical Design Report - Volume 2:
%Physics},
arXiv:1306.6352 [hep-ph].
%%CITATION = ARXIV:1306.6352;%%

%27
\bibitem{MHpLHCILCnewer} A.~Ferrari,
                         talk given at the 
                         {\em C$H^{\mbox{}^\pm}$\hspace{-2.5mm}arged 2006},
                         Uppsala, Sweden, September 2006. 

%28
\bibitem{ppttNNLO} M.~Czakon, P.~Fiedler and A.~Mitov,
  {\em Phys.\ Rev.\ Lett.} {\bf 110} (2013) 252004
  [arXiv:1303.6254 [hep-ph]].
  %%CITATION = ARXIV:1303.6254;%%

%29
\bibitem{Barnett:1987jw} R.~Barnett, H.~Haber and D.~Soper,
                         {\em Nucl.\ Phys.} {\bf B 306} (1988) 697.
                         %%CITATION = NUPHA,B306,697;%%

%30
\bibitem{Harlander:2011aa} R.~Harlander, M.~Kr\"amer and M.~Schumacher,
                           arXiv:1112.3478 [hep-ph].
                           %%CITATION = ARXIV:1112.3478;%%

%31
\bibitem{Flechl:2014wfa}
  M.~Flechl, R.~Klees, M.~Kr\"amer, M.~Spira and M.~Ubiali,
  arXiv:1409.5615 [hep-ph].
  %%CITATION = ARXIV:1409.5615;%%

%32
\bibitem{thdm-lhchxswg-reco}
        R.~Harlander, M.~Muhlleitner, J.~Rathsman, M.~Spira and O.~St{\aa}l,
        arXiv:1312.5571 [hep-ph].
        %%CITATION = ARXIV:1312.5571;%%

%33
\bibitem{hdecay} A.~Djouadi, J.~Kalinowski and M.~Spira,
  {\em Comput.\ Phys.\ Commun.} {\bf 108} (1998) 56
  [arXiv:hep-ph/9704448];
  %%CITATION = HEP-PH/9704448;%%
  A.~Djouadi, M.~M\"uhlleitner and M.~Spira,
  {\em Acta Phys.\ Polon.} {\bf B 38} (2007) 635
  [arXiv:hep-ph/0609292];
  %%CITATION = HEP-PH/0609292;%%
  M.~Spira,
  {\em Fortsch.\ Phys.} {\bf 46} (1998) 203
  [arXiv:hep-ph/9705337].
  %%CITATION = HEP-PH/9705337;%%

%34
\bibitem{2hdmc} D.~Eriksson, J.~Rathsman and O.~St{\aa}l,
  {\em Comput.\ Phys.\ Commun.} {\bf 181} (2010) 189
  [arXiv:0902.0851 [hep-ph]];
  %%CITATION = ARXIV:0902.0851;%%
  {\em Comput.\ Phys.\ Commun.} {\bf 181} (2010) 833.
  %%CITATION = CPHCB,181,833;%%

%35
\bibitem{feynhiggs} S.~Heinemeyer, W.~Hollik and G.~Weiglein,
                    {\em Comput. Phys. Commun.} {\bf 124} (2000) 76, 
                    [arXiv:hep-ph/9812320];
                    %%CITATION = HEP-PH 9812320;%%
           T.~Hahn, S.~Heinemeyer, W.~Hollik, H.~Rzehak and G.~Weiglein,
           {\em Comput.\ Phys.\ Commun.} {\bf 180} (2009) 1426;
           %%CITATION = CPHCB,180,1426;%%
           see: {\tt www.feynhiggs.de} .

%36
\bibitem{mhiggslong} S.~Heinemeyer, W.~Hollik and G.~Weiglein,
                     {\em Eur. Phys. J.} {\bf C 9} (1999) 343
                     [arXiv:hep-ph/9812472].
                     %%CITATION = HEP-PH 9812472;%%

%37
\bibitem{mhiggsAEC} G.~Degrassi, S.~Heinemeyer, W.~Hollik,
                    P.~Slavich and G.~Weiglein, 
                    {\em Eur. Phys. J.} {\bf C 28} (2003) 133
                    [arXiv:hep-ph/0212020].
                    %%CITATION = HEP-PH 0212020;%%

%38
\bibitem{mhcMSSMlong} M.~Frank, T.~Hahn, S.~Heinemeyer, W.~Hollik,  
                      H.~Rzehak and G.~Weiglein,
                      {\em JHEP} {\bf 0702} (2007) 047
                      [arXiv:hep-ph/0611326].
                      %%CITATION = HEP-PH 0611326;%%

%39
\bibitem{mhcMSSM2L} S.~Heinemeyer, W.~Hollik, H.~Rzehak and G.~Weiglein,
                    {\em Phys. Lett.} {\bf B 652} (2007) 300
                    [arXiv:0705.0746 [hep-ph]].
                    %%CITATION = ARXIV:0705.0746;%%

%40
\bibitem{Mh-logresum} T.~Hahn, S.~Heinemeyer, W.~Hollik, H.~Rzehak and
                      G.~Weiglein,  
{\em Phys. Rev. Lett.} {\bf 112} (2014) 141801
[arXiv:1312.4937 [hep-ph]].
%%CITATION = ARXIV:1312.4937;%%

%41
\bibitem{cpsh} J.~Lee, A.~Pilaftsis et al.,
               {\em Comput. Phys. Commun.} {\bf 156} (2004) 283
               [arXiv:hep-ph/0307377];
               %%CITATION = HEP-PH 0307377;%%
               J.~Lee, M.~Carena, J.~Ellis, A.~Pilaftsis and C.~Wagner,
               {\em Comput.\ Phys.\ Commun.} {\bf 180} (2009) 312
               [arXiv:0712.2360 [hep-ph]];
               %%CITATION = ARXIV:0712.2360;%%
               arXiv:1208.2212 [hep-ph].
               %%CITATION = ARXIV:1208.2212;%%

%42
\bibitem{deltamb1} R.~Hempfling,
                   {\em Phys. Rev.} {\bf D 49} (1994) 6168;
                   %%CITATION = PHRVA,D49,6168;%%
                   L.~Hall, R.~Rattazzi and U.~Sarid,
                   {\em Phys. Rev.} {\bf D 50} (1994) 7048,
                   hep-ph/9306309;
                   %%CITATION = HEP-PH 9306309;%%
                   M.~Carena, M.~Olechowski, S.~Pokorski and C.~Wagner,
                   {\em Nucl. Phys.} {\bf B 426} (1994) 269,
                   hep-ph/9402253.
                   %%CITATION = HEP-PH 9402253;%%

%43
\bibitem{deltamb2} M.~Carena, D.~Garcia, U.~Nierste and C.~Wagner,
                   {\em Nucl. Phys.} {\bf B 577} (2000) 577,
                   hep-ph/9912516.
                   %%CITATION = HEP-PH 9912516;%%

%44
\bibitem{db2l} D.~Noth and M.~Spira,
               {\em Phys.\ Rev.\ Lett.} {\bf 101} (2008)  181801
               [arXiv:0808.0087 [hep-ph]].

%45
\bibitem{benchmark2} M.~Carena, S.~Heinemeyer, C.~Wagner and G.~Weiglein, 
                     {\em Eur. Phys. J.} {\bf C 26} (2003) 601
                     [arXiv:hep-ph/0202167].
                     %%CITATION = HEP-PH 0202167;%%

%46
\bibitem{BR} A.~Denner, S.~Heinemeyer, I.~Puljak, D.~Rebuzzi and M.~Spira,
             {\em Eur.\ Phys.\ J.} {\bf C 71} (2011) 1753
             [arXiv:1107.5909 [hep-ph]].
             %%CITATION = ARXIV:1107.5909;%%

%47
\bibitem{benchmark4}
  M.~Carena, S.~Heinemeyer, O.~St{\aa}l, C.~Wagner and G.~Weiglein,
  {\em Eur.\  Phys.\  J.} {\bf C 73} (2013) 2552
  [arXiv:1302.7033 [hep-ph]].
  %%CITATION = ARXIV:1302.7033;%%


\end{thebibliography}

\end{document}


